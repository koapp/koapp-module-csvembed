(function() {
  'use strict';

  angular
    .module('csvembed', [])
    .controller('csvembedController', loadFunction);

  loadFunction.$inject = ['$scope', 'structureService', '$location'];

  function loadFunction($scope, structureService, $location) {
    //Register upper level modules
    structureService.registerModule($location, $scope, 'csvembed');

    // --- Start csvembedController content ---
    $scope.url = localStorage.getItem("csvPath");
    
    $scope.returnUrl = localStorage.getItem("returnUrl");
    
    //console.log($location.$$path);
    //console.log("url", $scope.url, "returnUrl", $scope.returnUrl);

    structureService.launchSpinner('.transitionloader');
  }
    
}());
